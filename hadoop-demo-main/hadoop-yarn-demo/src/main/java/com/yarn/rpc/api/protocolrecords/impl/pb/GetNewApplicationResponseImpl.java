package com.yarn.rpc.api.protocolrecords.impl.pb;

import com.yarn.rpc.api.protocolrecords.GetNewApplicationResponse;
import com.yarn.rpc.proto.YarnServiceProtos.GetNewApplicationResponseProto;

/**
 * @author wangzhihui
 * @date 2020/11/24 describe:
 */
public class GetNewApplicationResponseImpl extends GetNewApplicationResponse {
  GetNewApplicationResponseProto proto = GetNewApplicationResponseProto.getDefaultInstance();
  GetNewApplicationResponseProto.Builder builder = null;

  public GetNewApplicationResponseImpl(String applicationId) {
    builder = GetNewApplicationResponseProto.newBuilder();
    builder.addApplicationId(applicationId);
  }

  public GetNewApplicationResponseProto getProto() {
    return builder.build();
  }
}
