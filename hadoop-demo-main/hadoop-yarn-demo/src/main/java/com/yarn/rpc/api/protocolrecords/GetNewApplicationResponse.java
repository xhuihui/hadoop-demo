package com.yarn.rpc.api.protocolrecords;

import com.yarn.rpc.api.protocolrecords.impl.pb.GetNewApplicationResponseImpl;

/**
 * @author wangzhihui
 * @date 2020/11/24 describe:
 */
public abstract class GetNewApplicationResponse {
  public static GetNewApplicationResponse newInstance(
      String applicationId) {
    GetNewApplicationResponseImpl  getNewApplicationResponse = new GetNewApplicationResponseImpl(applicationId);
    return getNewApplicationResponse;
  }
}
