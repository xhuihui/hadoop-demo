package com.yarn.rpc.server;


import com.google.protobuf.RpcController;
import com.google.protobuf.ServiceException;
import com.yarn.rpc.api.ApplicationClientProtocol;
import com.yarn.rpc.api.protocolrecords.GetNewApplicationRequest;
import com.yarn.rpc.api.protocolrecords.GetNewApplicationResponse;
import com.yarn.rpc.api.protocolrecords.impl.pb.GetNewApplicationResponseImpl;
import com.yarn.rpc.proto.YarnServiceProtos.GetNewApplicationRequestProto;
import com.yarn.rpc.proto.YarnServiceProtos.GetNewApplicationResponseProto;
import org.apache.hadoop.yarn.exceptions.YarnException;

import java.io.IOException;
import java.util.Date;

/**
 * @author wangzhihui
 * @date 2020/11/24 describe:
 */
public class ClientRmProtocolImpl implements ApplicationClientProtocol {

  public GetNewApplicationResponse getNewApplication(GetNewApplicationRequest request)  {
    System.out.println("accept client request " );
    GetNewApplicationResponseImpl response = (GetNewApplicationResponseImpl) GetNewApplicationResponse
        .newInstance("application_" + System.currentTimeMillis());
    return response;
  }
}
