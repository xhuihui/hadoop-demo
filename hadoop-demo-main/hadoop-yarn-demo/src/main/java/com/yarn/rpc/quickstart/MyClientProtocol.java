package com.yarn.rpc.quickstart;

import org.apache.hadoop.ipc.VersionedProtocol;

/**
 * @author
 * @date 2019/4/9 08:35
 * @desc
 *定义RPC接口
 * 1. 继承 VersionedProtocol 描述版本信息
 * 2. PRC client 和 Server 之间版本号不同不能相互通信
 */
public interface MyClientProtocol extends VersionedProtocol {
     public    long versionID=1L;

     String echo(String name);

     int add(int v1, int v2);
}
