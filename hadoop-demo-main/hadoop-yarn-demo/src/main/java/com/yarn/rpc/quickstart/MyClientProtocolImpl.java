package com.yarn.rpc.quickstart;

import org.apache.hadoop.ipc.ProtocolSignature;

import java.io.IOException;

/**
 * @author
 * @date 2019/4/9 08:38
 * @desc
 * 接口实现类
 */
public class MyClientProtocolImpl  implements MyClientProtocol {

    @Override
    public String echo(String name) {
        return "Hello "+name;
    }

    @Override
    public int add(int v1, int v2) {
        return v1+v2;
    }


    //获取协议版本号
    @Override
    public long getProtocolVersion(String protocol, long clientVersion) throws IOException {

        return MyClientProtocol.versionID;
    }

    //获取协议签名
    @Override
    public ProtocolSignature getProtocolSignature(String protocol, long clientVersion, int clientMethodsHash) throws IOException {
        return  new ProtocolSignature( MyClientProtocol.versionID,null );
    }
}
