package com.yarn.rpc.api;


import com.yarn.rpc.proto.ApplicationClientProtocol;
import org.apache.hadoop.ipc.ProtocolInfo;

/**
 * @author wangzhihui
 * @date 2020/11/23 describe:
 */
//todo Version 信息从哪来的
@ProtocolInfo(protocolName = "com.yarn.rpc.api.ApplicationClientProtocolPB",
    protocolVersion = 1)
public interface ApplicationClientProtocolPB extends ApplicationClientProtocol.ApplicationClientProtocolService.BlockingInterface{

}
