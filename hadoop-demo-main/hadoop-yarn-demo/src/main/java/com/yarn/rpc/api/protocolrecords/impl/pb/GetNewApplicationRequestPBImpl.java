package com.yarn.rpc.api.protocolrecords.impl.pb;

import com.google.protobuf.TextFormat;
import com.yarn.rpc.api.protocolrecords.GetNewApplicationRequest;
import com.yarn.rpc.proto.YarnServiceProtos.GetNewApplicationRequestProto;

/**
 * @author wangzhihui
 * @date 2020/11/23 describe:
 */
public class GetNewApplicationRequestPBImpl extends GetNewApplicationRequest {
  GetNewApplicationRequestProto proto = GetNewApplicationRequestProto.getDefaultInstance();
  GetNewApplicationRequestProto.Builder builder = null;
  boolean viaProto = false;

  public GetNewApplicationRequestPBImpl() {
    builder = GetNewApplicationRequestProto.newBuilder();
  }


  public GetNewApplicationRequestPBImpl(GetNewApplicationRequestProto proto) {
    this.proto = proto;
    viaProto = true;
  }

  public GetNewApplicationRequestProto getProto() {
    proto = viaProto ? proto : builder.build();
    viaProto = true;
    return proto;
  }

  @Override
  public int hashCode() {
    return getProto().hashCode();
  }

  @Override
  public boolean equals(Object other) {
    if (other == null)
      return false;
    if (other.getClass().isAssignableFrom(this.getClass())) {
      return this.getProto().equals(this.getClass().cast(other).getProto());
    }
    return false;
  }

  @Override
  public String toString() {
    return TextFormat.shortDebugString(getProto());
  }
}
