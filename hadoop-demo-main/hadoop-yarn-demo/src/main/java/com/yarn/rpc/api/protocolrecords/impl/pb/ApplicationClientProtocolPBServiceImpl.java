package com.yarn.rpc.api.protocolrecords.impl.pb;


import com.google.protobuf.RpcController;
import com.google.protobuf.ServiceException;
import com.yarn.rpc.api.ApplicationClientProtocol;
import com.yarn.rpc.api.ApplicationClientProtocolPB;
import com.yarn.rpc.api.protocolrecords.GetNewApplicationRequest;
import com.yarn.rpc.api.protocolrecords.GetNewApplicationResponse;
import com.yarn.rpc.proto.YarnServiceProtos.GetNewApplicationResponseProto;
import com.yarn.rpc.proto.YarnServiceProtos.GetNewApplicationRequestProto;
import org.apache.hadoop.yarn.exceptions.YarnException;

import java.io.IOException;

public class ApplicationClientProtocolPBServiceImpl implements ApplicationClientProtocolPB {
  public ApplicationClientProtocol getReal() {
    return real;
  }

  public void setReal(ApplicationClientProtocol real) {
    this.real = real;
  }

  private ApplicationClientProtocol real;
  public ApplicationClientProtocolPBServiceImpl(ApplicationClientProtocol real) {
    this.real = real;
  }

  public ApplicationClientProtocolPBServiceImpl() {
  }

  public GetNewApplicationResponseProto getNewApplication(RpcController controller, GetNewApplicationRequestProto request)  {
    GetNewApplicationRequest getNewApplicationRequest = new GetNewApplicationRequestPBImpl(request);
    GetNewApplicationResponseImpl newApplication = (GetNewApplicationResponseImpl) real.getNewApplication(getNewApplicationRequest);
    return newApplication.getProto();
  }
}
