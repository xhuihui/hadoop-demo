package com.yarn.rpc.api;

import java.io.IOException;

import com.yarn.rpc.api.protocolrecords.GetNewApplicationRequest;
import com.yarn.rpc.api.protocolrecords.GetNewApplicationResponse;
import org.apache.hadoop.yarn.exceptions.YarnException;

/**
 * @author wangzhihui
 * @date 2020/11/23 describe:
 */
public interface ApplicationClientProtocol {

  public GetNewApplicationResponse getNewApplication(
      GetNewApplicationRequest request);

}
