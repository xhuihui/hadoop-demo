package com.yarn.rpc.quickstart;


import com.google.protobuf.BlockingService;
import com.yarn.rpc.api.ApplicationClientProtocolPB;
import com.yarn.rpc.api.protocolrecords.impl.pb.ApplicationClientProtocolPBServiceImpl;
import com.yarn.rpc.proto.ApplicationClientProtocol;
import com.yarn.rpc.server.ClientRmProtocolImpl;
import java.io.IOException;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.ipc.ProtobufRpcEngine;
import org.apache.hadoop.ipc.ProtobufRpcEngine.Server;
import org.apache.hadoop.ipc.RPC;

/**
 * @author
 * @date 2019/4/9 08:45
 * @desc 服务端入口
 */
public class MyServerMain {

  public static void main(String[] args) throws IOException, NoSuchMethodException, IllegalAccessException, InvocationTargetException, InstantiationException {
    //构造服务端
    Configuration config = new Configuration();
    RPC.setProtocolEngine(config, ApplicationClientProtocolPB.class, ProtobufRpcEngine.class);
    BlockingService blockingService = ApplicationClientProtocol.ApplicationClientProtocolService.newReflectiveBlockingService(new ApplicationClientProtocolPBServiceImpl(new ClientRmProtocolImpl()));
    RPC.Server server = new RPC.Builder(config).setProtocol(ApplicationClientProtocolPB.class)
        .setInstance(blockingService)
        .setBindAddress("localhost").setPort(8089).setNumHandlers(5).build();
    server.start();
  }
}
