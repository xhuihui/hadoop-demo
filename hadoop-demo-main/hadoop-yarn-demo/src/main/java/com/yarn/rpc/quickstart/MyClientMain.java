package com.yarn.rpc.quickstart;

import com.google.protobuf.ServiceException;
import com.yarn.rpc.api.ApplicationClientProtocolPB;
import com.yarn.rpc.api.protocolrecords.GetNewApplicationRequest;
import com.yarn.rpc.api.protocolrecords.impl.pb.GetNewApplicationRequestPBImpl;
import com.yarn.rpc.proto.YarnServiceProtos.GetNewApplicationResponseProto;
import java.io.IOException;
import java.net.InetSocketAddress;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.ipc.ProtobufRpcEngine;
import org.apache.hadoop.ipc.RPC;

/**
 * @author
 * @date 2019/4/9 08:41
 * @desc 客户端入口
 */
public class MyClientMain {
    public static void main(String[] args) throws IOException, ServiceException {

        InetSocketAddress inetSocketAddress = new InetSocketAddress("localhost",8089);
        Configuration config=   new Configuration();
        //构造客户端代理对象
        RPC.setProtocolEngine(config, ApplicationClientProtocolPB.class,ProtobufRpcEngine.class);
        ApplicationClientProtocolPB proxy=RPC.getProxy( ApplicationClientProtocolPB.class, 1L, inetSocketAddress, config );
        GetNewApplicationRequestPBImpl getNewApplicationRequest = (GetNewApplicationRequestPBImpl) GetNewApplicationRequest.newInstance();
        GetNewApplicationResponseProto newApplication = proxy
            .getNewApplication(null, getNewApplicationRequest.getProto());
        System.out.println("client "+ newApplication.getApplicationId(0));
    }
}
