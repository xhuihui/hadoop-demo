package com.yarn.rpc.api.protocolrecords;

import com.yarn.rpc.api.protocolrecords.impl.pb.GetNewApplicationRequestPBImpl;

/**
 * @author wangzhihui
 * @date 2020/11/23 describe:
 */
public abstract class GetNewApplicationRequest {
  public  static GetNewApplicationRequest newInstance(){
    return  new GetNewApplicationRequestPBImpl();
  }

}
