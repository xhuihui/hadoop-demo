package com.yarn.event;

import com.yarn.core003.evnenthandler.JobEventType;
import org.apache.hadoop.yarn.event.AbstractEvent;

/**
 * @author
 * @date 2019/5/5 08:46
 * @desc 定义job 事件
 */
public class JobEvent extends AbstractEvent<JobEventType> {
    private  String jobId;

    public String getJobId() {
        return jobId;
    }

    public JobEvent(String jobId, JobEventType jobEventType) {
        super( jobEventType );
        this.jobId=jobId;
    }

}
