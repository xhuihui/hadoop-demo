package com.yarn.event;

import com.yarn.core003.evnenthandler.JobEventType;
import com.yarn.core003.evnenthandler.TaskEventType;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.service.CompositeService;
import org.apache.hadoop.service.Service;
import org.apache.hadoop.yarn.conf.YarnConfiguration;
import org.apache.hadoop.yarn.event.AsyncDispatcher;
import org.apache.hadoop.yarn.event.Dispatcher;
import org.apache.hadoop.yarn.event.EventHandler;

/**
 * @author
 * @date 2019/5/5 08:49
 * @desc
 */
public class SimpleMRAppMaster  extends CompositeService {
    private Dispatcher dispatcher;//中央异步调度器
    private  String jobId;
    private  int taskNumbers;// job 包含的task个数
    private  String[] taskIds;// 该作业内包含的taskId


    public SimpleMRAppMaster(String name, String jobId, int taskNumbers) {
        super(name);
        this.jobId = jobId;
        this.taskNumbers = taskNumbers;
        this.taskIds = new String[taskNumbers];
        for(int  i=0;i<taskNumbers;i++){
              taskIds[i]=new String(jobId+"_task_"+i);
        }

    }

    @Override
    protected void serviceInit(Configuration conf) throws Exception {
        dispatcher=new AsyncDispatcher();//异步中央调度器
        // 注册Job 和Task 事件调度
        dispatcher.register(JobEventType.class,new JobEventDispatcher());
        dispatcher.register(TaskEventType.class,new TaskEventDispatcher());
        addService((Service) dispatcher);
        super.serviceInit(conf);
    }

    public Dispatcher getDispatcher() {
        return dispatcher;
    }

    private  class   JobEventDispatcher  implements EventHandler<JobEvent> {

        public void handle(JobEvent event) {
             if(event.getType() == JobEventType.JOB_KILL){
                 System.out.println("收到 kill  job event 请求，kill all tasks");
                  for(int i=0;i<taskNumbers;i++){
                       dispatcher.getEventHandler().handle(new TaskEvent(taskIds[i],TaskEventType.T_KILL));
                  }

             }else if(event.getType() == JobEventType.JOB_INIT){
                 System.out.println("收到 init  job event 请求，scheduling tasks");
                 for(int i=0;i<taskNumbers;i++){
                     dispatcher.getEventHandler().handle(new TaskEvent(taskIds[i],TaskEventType.T_SCHEDULE));
                 }
             }

        }
    }

    private  class TaskEventDispatcher implements  EventHandler<TaskEvent>{

        public void handle(TaskEvent event) {
            if(event.getType() == TaskEventType.T_KILL){
                System.out.println("Receive T_KILL  event of task"+event.getTaskId());
            }else if(event.getType() == TaskEventType.T_SCHEDULE){
                System.out.println("Receive T_SCHEDULE  event of task"+event.getTaskId());

            }
        }
    }

    public static void main(String[] args) throws Exception {
         String jobId="job_123123123_121";
         SimpleMRAppMaster simpleMRAppMaster=new SimpleMRAppMaster("Simple AppMaster",jobId,5);
        YarnConfiguration conf=new YarnConfiguration();
        simpleMRAppMaster.serviceInit(conf);
        simpleMRAppMaster.serviceStart();
        simpleMRAppMaster.getDispatcher().getEventHandler().handle(new JobEvent(jobId,JobEventType.JOB_KILL));
        simpleMRAppMaster.getDispatcher().getEventHandler().handle(new JobEvent(jobId,JobEventType.JOB_INIT));
    }

}
