package com.yarn.event;


import com.yarn.core003.evnenthandler.TaskEventType;
import org.apache.hadoop.yarn.event.AbstractEvent;


/**
 * @author
 * @date 2019/5/5 08:34
 * @desc 自定义Task事件
 */
public class TaskEvent extends AbstractEvent<TaskEventType>{
    private  String taskId;


    public String getTaskId() {
        return taskId;
    }

    public TaskEvent(String taskId, TaskEventType taskEventType) {
        super( taskEventType );
        this.taskId=taskId;
    }

}
