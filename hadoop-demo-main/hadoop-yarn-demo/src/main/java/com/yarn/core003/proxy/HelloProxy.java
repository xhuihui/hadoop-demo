package com.yarn.core003.proxy;

public class HelloProxy implements HelloInterface {
    private HelloInterface helloInterface = new HelloImpl();
    public void sayHello(String name) {
        System.out.println("helloInterface before");
        helloInterface.sayHello(name);
        System.out.println("helloInterface after");
    }
}
