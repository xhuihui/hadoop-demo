package com.yarn.core003.evnenthandler;

/**
 * @author
 * @date 2019/5/5 08:41
 * @desc 事件类型
 */
public enum TaskEventType {
    T_KILL,
    T_SCHEDULE;
}
