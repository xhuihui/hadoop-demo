package com.yarn.core003.hadooprpc;

import org.apache.hadoop.ipc.VersionedProtocol;

public interface ClientProtocol extends VersionedProtocol {
    public static final  long versionID = 1L;

    String  say(String name);

    int add(int value1, int value2);
}
