package com.yarn.core003.proxy;

public class HelloImpl  implements  HelloInterface{
    public void sayHello(String name) {
        System.out.println("hello "+name);
    }
}
