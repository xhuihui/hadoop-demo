package com.yarn.core003.hadooprpc;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.ipc.RPC;

import java.io.IOException;
import java.net.InetSocketAddress;

public class HadoopRpcClient
{
    public static void main(String[] args) throws IOException {
        ClientProtocol clientProxy = RPC.getProxy(
                ClientProtocol.class,
                ClientProtocol.versionID, 
                new InetSocketAddress("localhost", 8089), 
                new Configuration()
        );
        String sayResult = clientProxy.say("tom");
        System.out.println(sayResult);
        System.out.println("add result = "+clientProxy.add(1,1));
    }
}
