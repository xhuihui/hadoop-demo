package com.yarn.core003.evnenthandler;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.yarn.conf.YarnConfiguration;

public class SimpleAppMasterTest {
    public static void main(String[] args) throws Exception {
         String jobId ="job_775801";
         SimpleAppMaster simpleAppMaster = new SimpleAppMaster("simpleAm", jobId, 10);
        YarnConfiguration conf = new YarnConfiguration(new Configuration());
        simpleAppMaster.serviceInit(conf);
        simpleAppMaster.serviceStart();
        simpleAppMaster.getDispatcher().getEventHandler().handle(new JobEvent(JobEventType.JOB_INIT, jobId));
//        simpleAppMaster.getDispatcher().getEventHandler().handle(new JobEvent(JobEventType.JOB_KILL, jobId));
    }
}
