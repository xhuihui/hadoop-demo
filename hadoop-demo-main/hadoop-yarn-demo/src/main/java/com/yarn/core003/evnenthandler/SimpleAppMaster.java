package com.yarn.core003.evnenthandler;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.service.CompositeService;
import org.apache.hadoop.service.Service;
import org.apache.hadoop.yarn.event.AsyncDispatcher;
import org.apache.hadoop.yarn.event.Dispatcher;
import org.apache.hadoop.yarn.event.EventHandler;

public class SimpleAppMaster extends CompositeService {
    private Dispatcher dispatcher;//中央异步调度器
    private String jobId;
    private int taskNumber;//该作业包含的任务个数
    private String[] taskIds;//该作业内部包含的所有任务



    public SimpleAppMaster(String name) {
        super(name);
    }

    public SimpleAppMaster(String name, String jobId, int taskNumber) {
        super(name);
        this.jobId = jobId;
        this.taskNumber = taskNumber;
        taskIds =new String[taskNumber];
        for(int i=0 ; i< taskNumber; i++){
            taskIds[i] = "simpleAmTask_"+i;
        }
    }

    @Override
    protected void serviceStart() throws Exception {
        super.serviceStart();
    }

    @Override
    protected void serviceStop() throws Exception {
        super.serviceStop();
    }

    @Override
    protected void serviceInit(Configuration conf) throws Exception {

        dispatcher = new AsyncDispatcher();
        //注册事件调度器
//      dispatcher.register(JobEventType.class, new JobEventDispatcher());
//      dispatcher.register(TaskEventType.class, new TaskEventDispatcher());
        dispatcher.register(JobEventType.class, new JobStateMachine(jobId,dispatcher.getEventHandler()));
        addService((Service) dispatcher);
        super.serviceInit(conf);
    }

    public Dispatcher getDispatcher() {
        return dispatcher;
    }

 private  class JobEventDispatcher implements EventHandler<JobEvent> {

        public void handle(JobEvent jobEvent) {
            if(jobEvent.getType() == JobEventType.JOB_INIT){
                System.out.println("receiver job init jobId is "+ jobEvent.getJobId()+" scheduling tasks");
                for(int i=0; i< taskNumber;i++){
                    dispatcher.getEventHandler().handle(new TaskEvent(TaskEventType.T_SCHEDULE,taskIds[i]));
                }
            }else if(jobEvent.getType() == JobEventType.JOB_KILL){
                System.out.println("receiver job kill jobId is "+ jobEvent.getJobId()+" kliiing tasks");
                for(int i=0; i< taskNumber;i++){
                    dispatcher.getEventHandler().handle(new TaskEvent(TaskEventType.T_KILL,taskIds[i]));
                }
            }

        }
    }

    private class TaskEventDispatcher implements  EventHandler<TaskEvent>{

        public void handle(TaskEvent taskEvent) {
             if(taskEvent.getType() == TaskEventType.T_SCHEDULE){
                 System.out.println("receiver task "+ taskEvent.getTaskId() + " T_SCHEDULE");
             }else if(taskEvent.getType() == TaskEventType.T_KILL){
                 System.out.println("receiver task "+ taskEvent.getTaskId() + " T_Kill");
             }
        }
    }

}

