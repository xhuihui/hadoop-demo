package com.yarn.core003.evnenthandler;

/**
 * @author
 * @date 2019/5/5 08:49
 * @desc
 */
public enum JobEventType {
    JOB_KILL,
    JOB_INIT,
    JOB_START ,
    JOB_SETUP_COMPLETED ,
    JOB_COMPLETED ,
}
