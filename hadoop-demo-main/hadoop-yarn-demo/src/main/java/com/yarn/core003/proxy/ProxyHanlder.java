package com.yarn.core003.proxy;


import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;

public class ProxyHanlder implements InvocationHandler {
    private Object object;

    public ProxyHanlder(Object object) {
        this.object = object;
    }

    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        System.out.println("dynamic proxy before");
        method.invoke(object,args);
        System.out.println("dynamic proxy after");
        return null;
    }
}
