package com.yarn.core003.hadooprpc;

import org.apache.hadoop.ipc.ProtocolSignature;

import java.io.IOException;

public class ClientProtocolImpl implements ClientProtocol {
    public String say(String name) {
        return "hello "+name;
    }

    public int add(int value1, int value2) {
        return value1 + value2;
    }

    public long getProtocolVersion(String s, long l) throws IOException {
        return ClientProtocol.versionID;
    }

    //获取协议签名
    public ProtocolSignature getProtocolSignature(String s, long l, int i) throws IOException {
        return new ProtocolSignature(ClientProtocol.versionID,null);
    }
}
