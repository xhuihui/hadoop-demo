package com.yarn.core003.evnenthandler;

import org.apache.hadoop.yarn.event.AbstractEvent;

public class JobEvent extends AbstractEvent<JobEventType> {
    private String jobId;

    public JobEvent(JobEventType jobEventType, String jobId) {
        super(jobEventType);
        this.jobId = jobId;
    }

    public JobEvent(JobEventType jobEventType, long timestamp, String jobId) {
        super(jobEventType, timestamp);
        this.jobId = jobId;
    }

    public JobEvent(JobEventType jobEventType) {
        super(jobEventType);
    }


    public String getJobId() {
        return jobId;
    }
}