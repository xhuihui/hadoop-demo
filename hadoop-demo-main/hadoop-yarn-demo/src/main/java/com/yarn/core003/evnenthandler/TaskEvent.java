package com.yarn.core003.evnenthandler;

import org.apache.hadoop.yarn.event.AbstractEvent;

public class TaskEvent  extends AbstractEvent<TaskEventType> {
    private String taskId;

    public TaskEvent(TaskEventType taskEventType, String taskId) {
        super(taskEventType);
        this.taskId = taskId;
    }

    public TaskEvent(TaskEventType taskEventType) {
        super(taskEventType);
    }

    public String getTaskId() {
        return taskId;
    }
}
