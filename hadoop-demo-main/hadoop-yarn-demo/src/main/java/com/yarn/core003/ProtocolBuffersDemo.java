package com.yarn.core003;

import com.yarn.core003.ipc.proto.PersonProtos;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * 1、定义proto 接口文件格式
 * 2、编译生成java文件
 * 3、使用Protocol buffers 库api
 * **/
public class ProtocolBuffersDemo {
    public static void main(String[] args) throws IOException {
        //构建参数
        PersonProtos.Person.Builder personBuidler = PersonProtos.Person.newBuilder();
        personBuidler.setId(1101);
        personBuidler.setName("tom");
        personBuidler.setEmail("23123@qq.com");

        PersonProtos.Person.Phone.Builder phoneBuilder = PersonProtos.Person.Phone.newBuilder();
        phoneBuilder.setNumer1("198321312323");
        phoneBuilder.setType(1);

       personBuidler.addPhone(phoneBuilder);

        //builder 得到实体
        PersonProtos.Person person = personBuidler.build();
        //序列化输出
        FileOutputStream fos = new FileOutputStream("/opt/workspace/personInfo.txt");
        person.writeTo(fos);
        fos.close();

        //反序列化读取序列化结果
        FileInputStream fis = new FileInputStream("/opt/workspace/personInfo.txt");
        PersonProtos.Person personResult  = PersonProtos.Person.parseFrom(fis);
        System.out.printf(personResult.getName());
    }

}
