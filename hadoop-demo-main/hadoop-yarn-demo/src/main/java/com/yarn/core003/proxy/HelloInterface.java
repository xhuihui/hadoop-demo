package com.yarn.core003.proxy;

public interface HelloInterface {
    public void sayHello(String name);
}
