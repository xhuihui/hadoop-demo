package com.local.demo;

import java.util.Stack;

public class TreeDemo {

    public static void main(String[] args) {
        Node node1 = new Node(1);
        Node node2 = new Node(2);
        Node node3 = new Node(3);
        Node node4 = new Node(4);
        Node node5 = new Node(5);
        Node node6 = new Node(6);
        Node node7 = new Node(7);
        node1.left = node2; node1.right = node3;
        node2.left = node4; node2.right =node5;
        node3.left = node6; node3.right =node7;
        forHierarchyTravelTree(node1);

    }



    public static void forHierarchyTravelTree(Node root){
        Stack<Node> stack = new Stack<Node>();
        stack.add(root);
        while (!stack.isEmpty()){
            //travel 一层
            Stack<Node> newStak = new Stack<Node>();
            while (!stack.isEmpty()){
                Node node = stack.pop();
                System.out.printf(node.value+"__");
                newStak.push(node);
            }

            //push 入栈下一层数据
            while (!newStak.isEmpty()){
                Node tempNode = newStak.pop();
                if(tempNode.left != null){
                    stack.push(tempNode.left);
                }
                if(tempNode.right !=null){
                    stack.push(tempNode.right);
                }
            }
            //递归下一层
        }
    }

    static class Node {
        public int value;
        public Node left;
        public Node right;

        public Node(int value) {
            this.value = value;
        }
    }
}
