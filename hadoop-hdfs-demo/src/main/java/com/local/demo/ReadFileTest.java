package com.local.demo;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileStatus;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.fs.RemoteIterator;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;

public class ReadFileTest {

    private FileSystem fs;
    private Configuration conf;
    private String NAME_NODE_URI = "hdfs://localhost:9000";

    @Before
    public void initClient() throws URISyntaxException, IOException, InterruptedException {
        conf = new Configuration();
        conf.set("fs.defaultFS", NAME_NODE_URI);
        fs = FileSystem.get(new URI(NAME_NODE_URI+"/"),conf,"root");

    }



    @Test
    public void createDir() throws IOException {
        fs.mkdirs(new Path("/test"));
    }



    @Test
    public void  listFile() throws IOException {
        RemoteIterator<FileStatus> iterator = fs.listStatusIterator(new Path("/tmp"));
        while (iterator.hasNext()){
            FileStatus file = iterator.next();
            System.out.println(file.getPath());
        }

    }

    @After
    public void cose() throws IOException {
        if(null != fs){
            fs.close();
        }
    }



}
