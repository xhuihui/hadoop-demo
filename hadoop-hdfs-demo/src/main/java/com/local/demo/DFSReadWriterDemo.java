package com.local.demo;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileStatus;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.fs.Trash;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;

/**
 * 基础操作代码：
 * https://blog.csdn.net/qq342643414/article/details/59138532
 * */
public class DFSReadWriterDemo {
    private FileSystem fs;
    private Configuration conf;
    private String NAME_NODE_URI = "hdfs://localhost:9000";

    @Before
    public void initClient() throws URISyntaxException, IOException, InterruptedException {
         conf = new Configuration();
        conf.set("fs.defaultFS", NAME_NODE_URI);
        fs = FileSystem.get(new URI(NAME_NODE_URI+"/"),conf,"root");

    }


    @Test
    public void  listFile() throws IOException {
        FileStatus[] fileStatuses = fs.listStatus(new Path("/tmp"));
        for(FileStatus fileStatus :fileStatuses){
            System.out.println(fileStatus.getPath().getName());
        }
    }

    @Test
    public void delete() throws IOException {
       boolean result = Trash.moveToAppropriateTrash(fs,new Path("/tmp/hadoop-wzh-namenode-localhost.out"),null);
        System.out.println(result);
    }



}
