package com.local.demo;

import org.apache.commons.lang.StringUtils;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FSDataInputStream;
import org.apache.hadoop.fs.FSDataOutputStream;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.hdfs.DFSClient;
import org.apache.hadoop.hdfs.client.HdfsDataInputStream;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URI;
import java.net.URISyntaxException;

public class DFSFileOperator {
    private FileSystem fs;
    private Configuration conf;
    private String NAME_NODE_URI = "hdfs://localhost:9000";
    private Path TEST_FILE = new Path("/tmp/append.txt");

    static {
        String baseLib = System.getProperty("java.library.path");
        baseLib = baseLib + ":/opt/software/hadoop-3.1.0/lib/native/";
        System.setProperty("java.library.path",baseLib);
        System.out.println(System.getProperty("java.library.path"));

    }

    @Before
    public void initClient() throws URISyntaxException, IOException, InterruptedException {
        conf = new Configuration();
//        conf.addResource(new Path("/opt/software/hadoop-3.1.0/etc/hadoop/hdfs-site.xml"));
        conf.set("fs.defaultFS", NAME_NODE_URI);
        conf.set("dfs.client.read.shortcircuit","true");
        conf.set("dfs.domain.socket.path","/opt/software/hadoop_data_3.x/dn_socket");

        fs = FileSystem.get(new URI(NAME_NODE_URI + "/"), conf, "root");
    }


    @Test
    public void delete() throws IOException, URISyntaxException {
        DFSClient dfsClient = new DFSClient(new URI(NAME_NODE_URI),conf);
//        boolean result = fs.delete(TEST_FILE);
//        System.out.println(result);
        boolean result = dfsClient.delete("/readme.txt");
        System.out.println(result);
    }
    @Test
    public void createAndWriteFile() throws IOException {
        FSDataOutputStream fsDataOutputStream = fs.create(TEST_FILE,true);
        for(int i=0; i<10000;i++){
            fsDataOutputStream.writeBytes("this is simple test\n");
        }
        fsDataOutputStream.close();
    }

    @Test
    public void readFile() throws IOException {
//      实际返回  HdfsDataInputStream
        FSDataInputStream open = fs.open(TEST_FILE);
        HdfsDataInputStream read = (HdfsDataInputStream) open;
        read.read();
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(open));
        String line =null;
        char[] chars = new char[1024*1024*10];
        bufferedReader.read(chars);
        while (  -1 != bufferedReader.read(chars)){
            System.out.println(chars.length);
        }
//        while (!StringUtils.isBlank(line = bufferedReader.readLine())){
//            System.out.println(line);
//        }
    }

    @Test
    public void copyToLocal() throws IOException {
         fs.copyToLocalFile(TEST_FILE,new Path("/opt/software/hadoop_data_3.x/local_test/"));
    }

    @Test
    public void appendFile() throws IOException {
        Path appendFile = new Path("/tmp/append.txt");
        if (!fs.exists(appendFile)) {
            FSDataOutputStream fsDataOutputStream = fs.create(appendFile);
            fsDataOutputStream.close();
        }
        FSDataOutputStream fsDataOutputStream =  fs.append(appendFile);
        for(int i=0; i<9000000;i++){
            fsDataOutputStream.writeBytes("this is simple test\n");
        }
        fsDataOutputStream.close();
        fs.close();
    }

    @After
    public void close() throws IOException {
        if (null != fs) {
             fs.close();
        }
    }
}
