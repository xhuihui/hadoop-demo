package org.feiyu.com.rpc.protocolPB;

import com.google.protobuf.RpcController;
import com.google.protobuf.ServiceException;
import org.feiyu.com.rpc.protocol.SimpleProtocol;
import org.feiyu.com.rpc.protocol.proto.RPCDemoProtocolProtos;
import org.feiyu.com.rpc.protocol.proto.RPCDemoProtocolProtos.SayHelloResponseProto;

public class SimpleProtocolServerSideTranslatorPB implements SimpleProtocolProtosPB {

  final private SimpleProtocol server;

  public SimpleProtocolServerSideTranslatorPB(SimpleProtocol server) {
    this.server = server;
  }

  @Override
  public SayHelloResponseProto sayHello(RpcController controller,
      RPCDemoProtocolProtos.SayHelloRequestProto request) throws ServiceException {
    String result = server.sayHello(request.getName());
    SayHelloResponseProto reponse = SayHelloResponseProto.newBuilder().setMsg(result).build();
    return reponse;
  }
}
