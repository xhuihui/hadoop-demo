package org.feiyu.com.rpc.client;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.util.concurrent.atomic.AtomicBoolean;

import com.google.protobuf.ServiceException;
import org.feiyu.com.rpc.protocol.SimpleProtocol;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.security.UserGroupInformation;

public class MyClient {
  public static void main(String[] args) throws IOException, ServiceException {
    InetSocketAddress address = new InetSocketAddress("127.0.0.1", 50070);
    Configuration configuration = new Configuration();
    UserGroupInformation ugi = UserGroupInformation.getCurrentUser();
    AtomicBoolean fallbackToSimpleAut = new AtomicBoolean(true);
    SimpleProtocol client =
        ProxiesClientUtils.createProxyWithAlignmentContext(address, configuration, ugi,
            fallbackToSimpleAut);
    String result = client.sayHello("tom");
    System.out.printf("result=" + result);
  }
}
