package org.feiyu.com.rpc.client;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.util.concurrent.atomic.AtomicBoolean;

import org.feiyu.com.rpc.protocol.SimpleProtocol;
import org.feiyu.com.rpc.protocol.proto.RPCDemoProtocolProtos;
import org.feiyu.com.rpc.protocolPB.SimpleProtocolProtosPB;
import org.feiyu.com.rpc.protocolPB.SimpleProtocolTranslatorPB;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.io.retry.RetryPolicies;
import org.apache.hadoop.io.retry.RetryPolicy;
import org.apache.hadoop.ipc.ProtobufRpcEngine;
import org.apache.hadoop.ipc.RPC;
import org.apache.hadoop.net.NetUtils;
import org.apache.hadoop.security.UserGroupInformation;

/**
 * 生成客户端代理对象工具类
 */
public class ProxiesClientUtils {

  public static SimpleProtocol createProxyWithAlignmentContext(InetSocketAddress address,
      Configuration conf, UserGroupInformation ugi, AtomicBoolean fallbackToSimpleAuth)
      throws IOException {
    //指定协议引擎
    RPC.setProtocolEngine(conf, SimpleProtocolProtosPB.class, ProtobufRpcEngine.class);

    //失败重试策略
    RetryPolicy defaultPolicy = RetryPolicies.TRY_ONCE_THEN_FAIL;

    final long version = RPC.getProtocolVersion(SimpleProtocolProtosPB.class);
    RPCDemoProtocolProtos.SimpleProtocol.BlockingInterface proxy =
        RPC.getProtocolProxy(SimpleProtocolProtosPB.class, version, address, ugi, conf,
            NetUtils.getDefaultSocketFactory(conf), org.apache.hadoop.ipc.Client.getTimeout(conf),
            defaultPolicy, fallbackToSimpleAuth).getProxy();

    return new SimpleProtocolTranslatorPB(proxy);

  }

}
