package org.feiyu.com.rpc.protocolPB;

import org.feiyu.com.rpc.protocol.proto.RPCDemoProtocolProtos;

import org.apache.hadoop.ipc.ProtocolInfo;

/**
 * 协议名称版本号基础信息，在数据传输时会校验
 * */
@ProtocolInfo(protocolName = "org.feiyu.com.rpc.protocolPB.SimpleProtocolProtosPB",
    protocolVersion = 1)
public interface SimpleProtocolProtosPB extends
    RPCDemoProtocolProtos.SimpleProtocol.BlockingInterface
    {
}
