package org.feiyu.com.rpc.server;

import java.io.IOException;

import org.feiyu.com.rpc.protocol.SimpleProtocol;
import org.feiyu.com.rpc.protocol.proto.RPCDemoProtocolProtos;
import org.feiyu.com.rpc.protocolPB.SimpleProtocolProtosPB;
import org.feiyu.com.rpc.protocolPB.SimpleProtocolServerSideTranslatorPB;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.ipc.ProtobufRpcEngine;

import com.google.protobuf.BlockingService;

import org.apache.hadoop.ipc.RPC;

public class MyServer implements SimpleProtocol {
  public static RPC.Server RPC_SERVER;
  public static final String BIND_HOST = "127.0.0.1";
  public static final int PORT = 50070;

  public static void main(String[] args) throws IOException {

    //初始化服务
    MyServer myServer = new MyServer();

    //启动服务
    myServer.start();

  }

  @Override
  public String sayHello(String name) {
    System.out.println("accept from client...");
    return "Hello " + name;
  }

  public MyServer() throws IOException {

    //1. RPC 服务基础信息配置
    Configuration conf = new Configuration();

    //2.绑定接口协议,和序列化引擎
    RPC.setProtocolEngine(conf, SimpleProtocolProtosPB.class, ProtobufRpcEngine.class);
    BlockingService simpleService =
        RPCDemoProtocolProtos.SimpleProtocol.newReflectiveBlockingService(
            new SimpleProtocolServerSideTranslatorPB(this));

    RPC_SERVER =
        new RPC.Builder(conf).setProtocol(SimpleProtocolProtosPB.class).setInstance(simpleService)
            .setBindAddress(BIND_HOST).setPort(PORT).setNumHandlers(2).build();

    //剩余接口服务可以手动指定
    //RPC.setProtocolEngine(conf, SimpleProtocolProtosPB.class, ProtobufRpcEngine2.class);
    //RPC_SERVER.addProtocol(RPC.RpcKind.RPC_PROTOCOL_BUFFER, SimpleProtocolProtosPB.class, new SimpleProtocolServerSideTranslatorPB(this));

  }

  void start() {
    RPC_SERVER.start();
  }
}
