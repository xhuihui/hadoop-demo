package org.feiyu.com.rpc.protocolPB;

import com.google.protobuf.ServiceException;
import org.feiyu.com.rpc.protocol.SimpleProtocol;
import org.feiyu.com.rpc.protocol.proto.RPCDemoProtocolProtos;
import org.feiyu.com.rpc.protocol.proto.RPCDemoProtocolProtos.SayHelloRequestProto;

public class SimpleProtocolTranslatorPB implements SimpleProtocol {


   final private RPCDemoProtocolProtos.SimpleProtocol.BlockingInterface proxy;

  public SimpleProtocolTranslatorPB(RPCDemoProtocolProtos.SimpleProtocol.BlockingInterface proxy) {
    this.proxy = proxy;
  }

  @Override
  public String sayHello(String name) throws ServiceException {
    SayHelloRequestProto requestProto = SayHelloRequestProto.newBuilder().setName(name).build();
    RPCDemoProtocolProtos.SayHelloResponseProto responseProto = proxy.sayHello(null, requestProto);
    return responseProto.getMsg();
  }
}
