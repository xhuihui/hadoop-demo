package org.feiyu.com.rpc.protocol;

import com.google.protobuf.ServiceException;

public interface SimpleProtocol {

  public String sayHello(String name) throws ServiceException;

}
